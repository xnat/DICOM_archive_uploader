# Xnat .tar.gz-Uploader

This project contains a shell-script which uploads all compressed dicom files in a specified folder.

## Files

* many-decom-targz-files-xnat-importer *shell-script, which does all the work*

## Prerequisites

* Bash
* cURL (mostly pre-installed on Linux-systems)

## How to use

* alter the fields in the shell-skript for your demands:
   * `USER=`... *your xnat-user-name*
   * `PASSWORD=`... *the password of your user*
   * `HOST=`... *the adress of your xnat-server*
   * `PROJECT=`... *the project, where you want to upload the decom-images*
   * `DIRECTORY=`... *the directory of the files you want to upload*
* run the shell-script
